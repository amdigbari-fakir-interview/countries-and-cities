import { ICity, LocalStoragePinnedCitiesType } from 'types';
import { SORT_ORDERS } from 'utils/constants';

export const PINNED_CITIES_LOCAL_STORAGE_KEY = 'pinned_cities';
export const getPinnedCities = () => {
    if (typeof window !== 'undefined') {
        const stringifiedItems = window.localStorage.getItem(PINNED_CITIES_LOCAL_STORAGE_KEY);
        return stringifiedItems ? (JSON.parse(stringifiedItems) as LocalStoragePinnedCitiesType) : [];
    }

    return [];
};

export const isCityPinned = (city: ICity['id']) => {
    return getPinnedCities().includes(city);
};

export const compareCities = (a: ICity, b: ICity, sort?: SORT_ORDERS) => {
    // NOTE: To sort based on the pin value
    const pinDifference = Number(isCityPinned(b.id)) - Number(isCityPinned(a.id));

    if (pinDifference !== 0) return pinDifference;

    if (sort && [SORT_ORDERS.ASC, SORT_ORDERS.DESC].includes(sort)) {
        return (sort === SORT_ORDERS.ASC ? 1 : -1) * a.title.toLowerCase().localeCompare(b.title.toLocaleLowerCase());
    }

    return 0;
};
