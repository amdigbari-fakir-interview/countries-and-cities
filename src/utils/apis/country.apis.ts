import { CountriesListApiRequestParamsType, CountriesListApiResponseType } from 'types';
import { sleep } from 'utils/invocation';

export const FETCH_COUNTRIES_LIST_API = '/api/countries';
export const fetchCountiesList: (params: CountriesListApiRequestParamsType) => Promise<CountriesListApiResponseType['data']> = async ({
    query = '',
}) => {
    try {
        // NOTE: Just to show a loading indicator
        await sleep(2000);

        const searchParams = new URLSearchParams({ query });

        const response = await fetch(`${FETCH_COUNTRIES_LIST_API}?${searchParams.toString()}`).then<CountriesListApiResponseType>((res) =>
            res.json(),
        );

        return response.data;
    } catch (e) {
        throw e;
    }
};
