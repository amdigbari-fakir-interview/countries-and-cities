import { CountryCitiesListApiRequestParamsType, CountryCitiesListApiResponseType } from 'types';
import { compareCities } from 'utils/city';
import { PAGINATED_RESPONSE_PER_PAGE } from 'utils/constants';
import { sleep } from 'utils/invocation';

export const FETCH_COUNTRIES_CITIES_LIST_API = '/api/countries/cities';
export const fetchCountiesCitiesList: (
    params: CountryCitiesListApiRequestParamsType,
) => Promise<CountryCitiesListApiResponseType> = async ({ countries, sort, query = '', page = 1 }) => {
    try {
        // NOTE: Just to show a loading indicator
        await sleep(2000);

        if (!countries?.length) {
            return Promise.reject();
        } else {
            const searchParams = new URLSearchParams({ query });
            countries.forEach((country) => searchParams.append('countries', country));
            const response = await fetch(
                `${FETCH_COUNTRIES_CITIES_LIST_API}?${searchParams.toString()}`,
            ).then<CountryCitiesListApiResponseType>((res) => res.json());

            let cities = response.data;

            const totalPages = Math.ceil(cities.length / PAGINATED_RESPONSE_PER_PAGE);

            if (page > totalPages) {
                return { status: true, data: [], meta: { pagination: { is_ended: true } } }; // NOTE: 404 Error status code
            }

            cities.sort((a, b) => compareCities(a, b, sort));

            const startIndex = (page - 1) * PAGINATED_RESPONSE_PER_PAGE;
            const endIndex = page * PAGINATED_RESPONSE_PER_PAGE;
            cities = cities.slice(startIndex, endIndex);

            return { status: true, data: cities, meta: { pagination: { is_ended: page >= totalPages } } };
        }
    } catch (e) {
        throw e;
    }
};
