import { AdListApiRequestParamsType, AdListApiResponseType } from 'types';
import { DEFAULT_AD_COUNT_PER_REQUEST } from 'utils/constants';
import { sleep } from 'utils/invocation';

export const FETCH_ADS_API = '/api/ads';
export const fetchAds: (params: AdListApiRequestParamsType) => Promise<AdListApiResponseType['data']> = async ({
    per_page = DEFAULT_AD_COUNT_PER_REQUEST,
    page = 1,
}) => {
    try {
        // NOTE: Just to show a loading indicator
        await sleep(2000);

        const searchParams = new URLSearchParams({ per_page: per_page.toString(), page: page.toString() });

        const response = await fetch(`${FETCH_ADS_API}?${searchParams.toString()}`).then<AdListApiResponseType>((res) => res.json());

        return response.data;
    } catch (e) {
        throw e;
    }
};
