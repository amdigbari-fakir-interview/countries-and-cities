// NOTE: Make sure this number can divide by 4. This is because every fifth item should be an ad
export const PAGINATED_RESPONSE_PER_PAGE = 20;
// NOTE: 5 is because every fifth item is an Ad
// NOTE: 20 items needs 5 ads in positions 5, 10, 15, 20, 25 = 20 items + 5 ads
export const AD_POSITION_SPLITTER = 5;
export const DEFAULT_AD_COUNT_PER_REQUEST = Math.floor(PAGINATED_RESPONSE_PER_PAGE / (AD_POSITION_SPLITTER - 1));
