export enum SORT_ORDERS {
    ASC = 'asc',
    DESC = 'desc',
    No_Sort = 'no_sort',
}
