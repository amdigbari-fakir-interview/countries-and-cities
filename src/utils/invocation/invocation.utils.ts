import { FunctionType } from 'types';

export const sleep = async (timeout: number) => {
    await new Promise((r) => setTimeout(r, timeout));
};

export function debounce(callback: FunctionType, timeout = 250): FunctionType {
    let timeoutRef: NodeJS.Timeout | undefined;

    return function (this: unknown, ...args: Array<unknown>) {
        const context = this;

        clearTimeout(timeoutRef);
        timeoutRef = setTimeout(function () {
            timeoutRef = undefined;
            callback.apply(context, args);
        }, timeout);
    };
}
