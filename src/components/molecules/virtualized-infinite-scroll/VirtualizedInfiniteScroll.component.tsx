import classNames from 'classnames';
import { Spinner } from 'components/atoms';
import { FC, useState } from 'react';
import { Virtuoso, VirtuosoProps } from 'react-virtuoso';

import { VIRTUALIZED_INFINITE_SCROLL_THRESHOLD } from './VirtualizedInfiniteScroll.constants';
import { VirtualizedInfiniteScrollItemBaseType, VirtualizedInfiniteScrollProps } from './VirtualizedInfiniteScroll.models';
import styles from './VirtualizedInfiniteScroll.module.scss';

/**
 * Wrap the component with a fixed size tag with the overflow hidden css property.
 *
 * Most of the props for this component is like Virtuoso in react-virtuoso component.
 */
export function VirtualizedInfiniteScroll<T extends VirtualizedInfiniteScrollItemBaseType>({
    items,
    loadMore,
    renderItem,
    isEnded,
    threshold = VIRTUALIZED_INFINITE_SCROLL_THRESHOLD,
    className,
    components = {},
    ...restProps
}: VirtualizedInfiniteScrollProps<T>): ReturnType<FC> {
    const [loading, setLoading] = useState(false);

    const itemContent: VirtuosoProps<T, unknown>['itemContent'] = (_, item) => renderItem(item);

    const loadItems = () => {
        if (!isEnded) {
            setLoading(true);
            loadMore().finally(() => setLoading(false));
        }
    };

    const Loading = () => (
        <div className={styles['loading']}>
            <Spinner />
        </div>
    );

    return (
        <>
            {(!!items.length || !loading) && (
                <Virtuoso
                    {...restProps}
                    data={items}
                    endReached={loadItems}
                    overscan={threshold}
                    className={classNames(className, 'w-full h-full')}
                    itemContent={itemContent}
                    components={{ ...components, Footer: loading ? Loading : components.Footer }}
                />
            )}
        </>
    );
}
