import { FC } from 'react';
import { VirtuosoProps } from 'react-virtuoso';

export type VirtualizedInfiniteScrollItemBaseType = { id: string };

export type VirtualizedInfiniteScrollProps<T extends VirtualizedInfiniteScrollItemBaseType> = Omit<
    VirtuosoProps<T, unknown>,
    'data' | 'endReached' | 'overscan' | 'itemContent'
> & {
    items: Array<T>;
    renderItem: (item: T) => ReturnType<FC>;
    isEnded: boolean;
    loadMore: () => Promise<Array<T>>;
    threshold?: number;
};
