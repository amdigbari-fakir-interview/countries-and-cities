import { SearchBarProps } from 'components/atoms';
import { InputProps } from 'components/atoms/input';
import { ComponentPropsWithoutRef, ReactNode } from 'react';
import { GenericObjectBaseType } from 'types';

export type DropdownSelectorProps<T extends GenericObjectBaseType> = Omit<ComponentPropsWithoutRef<'div'>, 'id'> & {
    items: Array<T>;
    selectedItems?: Array<T>;
    setSelectedItems: (items?: Array<T>) => void;
    getTitle: (item: T) => string;
    renderItem?: (item: T, selected?: boolean) => ReactNode;
    label: string;
    id: string;
    fullWidth?: boolean;
    inputProps?: Omit<InputProps, 'label' | 'staticIcon' | 'fullWidth' | 'icon' | 'onChange' | 'id'> & { id?: string };
    onClear?: InputProps['onClear'];
    search: SearchBarProps['search'];
};
