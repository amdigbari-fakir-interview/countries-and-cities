import classNames from 'classnames';
import { Button, Input, ListItem, ListItemProps, SearchBar } from 'components/atoms';
import { FC, FocusEvent, useEffect, useId, useRef, useState } from 'react';
import { useClickAway } from 'react-use';
import { GenericObjectBaseType } from 'types';

import { DropdownSelectorProps } from './DropdownSelector.models';
import styles from './DropdownSelector.module.scss';

export function DropdownSelector<T extends GenericObjectBaseType>({
    label,
    id: idProp,
    className,
    fullWidth,
    inputProps,
    onFocus,
    onClear,
    search,
    items,
    getTitle,
    renderItem,
    setSelectedItems,
    selectedItems,
    ...restProps
}: DropdownSelectorProps<T>): ReturnType<FC> {
    const dropdownWrapperRef = useRef<HTMLDivElement>(null);
    const dropdownRef = useRef<HTMLDivElement>(null);
    const inputRef = useRef<HTMLInputElement>(null);

    const [isOpen, setIsOpen] = useState(false);
    const [values, setValues] = useState(selectedItems);

    const randomId = useId();
    const id = idProp || randomId;

    const selectedItemsJoinedTitle = selectedItems?.map((item) => getTitle(item)).join(', ') || '';

    const closeDropdown = () => {
        setIsOpen(false);

        inputRef.current?.blur();
    };

    useClickAway(dropdownWrapperRef, () => {
        closeDropdown();
    });

    const handleFocus = (e: FocusEvent<HTMLDivElement>) => {
        setIsOpen(true);
        onFocus?.(e);
    };

    const itemClickHandler: (item: T) => ListItemProps['onClick'] = (item) => () => {
        setValues((valuesList) =>
            valuesList?.find((v) => v.id === item.id) ? valuesList.filter((v) => v.id !== item.id) : [...(valuesList || []), item],
        );
    };

    const render: NonNullable<DropdownSelectorProps<T>['renderItem']> = (item, selected) => (
        <ListItem key={item.id} className={styles['dropdown__list_item']} onClick={itemClickHandler(item)}>
            {renderItem ? renderItem(item, selected) : <p className={styles['dropdown__list_default-item']}>{getTitle(item)}</p>}
        </ListItem>
    );

    const submit = () => {
        setSelectedItems(values);

        closeDropdown();
    };

    useEffect(() => {
        setValues(selectedItems);
    }, [selectedItems]);

    return (
        <div
            id={id}
            className={classNames(styles.dropdown__wrapper, className, { 'w-full': fullWidth })}
            ref={dropdownWrapperRef}
            {...restProps}>
            <Input
                {...inputProps}
                value={selectedItemsJoinedTitle}
                label={label}
                id={inputProps?.id || `dropdown-selector-input-${id}`}
                readOnly
                fullWidth={fullWidth}
                className={classNames(styles.dropdown__input, inputProps?.className)}
                onFocus={handleFocus}
                forceFocus={isOpen}
                onClear={onClear}
                ref={inputRef}
            />

            <div
                id={`dropdown-selector-result-box-${id}`}
                className={classNames(styles.dropdown, { [styles['dropdown--show']]: isOpen })}
                ref={dropdownRef}>
                <SearchBar placeholder="Search" id={`dropdown-selector-search-bar-${id}`} search={search} fullWidth />

                <ul className={styles['dropdown__list']}>{items.map((item) => render(item, !!values?.find((v) => v.id === item.id)))}</ul>

                <Button fullWidth className={styles['dropdown__submit']} disabled={!values?.length} onClick={submit}>
                    Select Countries
                </Button>
            </div>
        </div>
    );
}
