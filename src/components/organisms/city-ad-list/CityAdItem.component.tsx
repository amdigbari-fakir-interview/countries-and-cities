import classNames from 'classnames';
import { Button } from 'components/atoms';
import { FC, useState } from 'react';

import { CityItemProps } from './CityAdList.models';
import styles from './CityAdList.module.scss';

export const CityAdItem: FC<CityItemProps> = ({ item, className, pinCity, isPinned }) => {
    const [loading, setLoading] = useState(false);

    const pin = async () => {
        setLoading(true);
        try {
            await pinCity(item.id);
        } finally {
            setLoading(false);
        }
    };

    return (
        <li
            className={classNames(
                styles['container_list__item'],
                { [styles['container_list__pinned-item']]: isPinned, [styles['container_list__ad-item']]: item.isAd },
                className,
            )}>
            <div className={styles['container_list__item-title-avatar']}>
                {/* {city.image && <Avatar src={city.image} alt={`${city.title} image`} />} */}

                <span>{item.title}</span>
            </div>

            {item.isAd && <span>Advertisement</span>}
            {!item.isAd && (
                <Button className={styles['container_list__pin-button']} loading={loading} onClick={pin}>
                    {isPinned ? 'Unpin' : 'Pin'}
                </Button>
            )}
        </li>
    );
};
