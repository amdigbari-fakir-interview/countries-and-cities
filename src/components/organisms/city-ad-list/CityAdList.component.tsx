import classNames from 'classnames';
import { VirtualizedInfiniteScroll, VirtualizedInfiniteScrollProps } from 'components/molecules/virtualized-infinite-scroll';
import { FC, useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { useLocalStorage } from 'react-use';
import { CitiesSetValueContext, CitiesValueContext } from 'stores/cities';
import { CountriesValueContext } from 'stores/countries';
import { CountryCitiesListApiRequestParamsType, ICity, LocalStoragePinnedCitiesType } from 'types';
import { fetchCountiesCitiesList } from 'utils/apis';
import { fetchAds } from 'utils/apis/ad.apis';
import { PINNED_CITIES_LOCAL_STORAGE_KEY, compareCities } from 'utils/city';
import { AD_POSITION_SPLITTER, DEFAULT_AD_COUNT_PER_REQUEST } from 'utils/constants';
import { sleep } from 'utils/invocation';

import { CityAdItem } from './CityAdItem.component';
import { CityItemProps, CityListHeaderProps, CityListProps, ICityAd } from './CityAdList.models';
import styles from './CityAdList.module.scss';
import { cityListSortFilterArray } from './CityAdList.utils';
import { CityListHeader } from './CityAdListHeader.component';

export const CityList: FC<CityListProps> = ({ className, ...restProps }) => {
    const { selectedCountries } = useContext(CountriesValueContext);
    const { cities, ads } = useContext(CitiesValueContext);
    const { setCities, setAds } = useContext(CitiesSetValueContext);

    /**
     * @params sortIndex Defining a constant array for sort values and change the sort approach to the next one whenever the button clicked
     * @params page The last page of the infinite scroll component
     */
    const [filters, setFilters] = useState<CityListHeaderProps['filters']>({ sortIndex: 0, page: 1 });

    const [query, setQuery] = useState('');

    const [loading, setLoading] = useState(false);

    // NOTE: Set it to true at first the change it after receiving initial data
    const [isEnded, setEnded] = useState(true);

    // NOTE: The stored value in the localStorage for the pinned cities
    const [pinnedCities, setPinnedCities] = useLocalStorage<LocalStoragePinnedCitiesType>(PINNED_CITIES_LOCAL_STORAGE_KEY);

    const sortedItems = useMemo<CityListHeaderProps['sortedItems']>(() => {
        const newItems: Array<ICityAd> = [...(cities || [])];

        // NOTE: I'm sure about this type cast because I only added cities to it in the previous line
        newItems.sort((a, b) => compareCities(a as ICity, b as ICity, cityListSortFilterArray[filters.sortIndex].sort));

        // NOTE: Push ads to 4, 9, 14, ... indexes
        ads?.forEach((ad, index) => newItems.splice((index + 1) * AD_POSITION_SPLITTER - 1, 0, ad));

        return newItems;

        // NOTE: Just want to change the cities sort when these fields has been changed
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pinnedCities, cities, filters, ads]);

    const togglePin: CityItemProps['pinCity'] = async (city) => {
        // NOTE: Just to show a loading indicator
        await sleep(1000);
        setPinnedCities(pinnedCities?.includes(city) ? pinnedCities.filter((c) => c !== city) : [...(pinnedCities || []), city]);
    };

    const isCityPinned = useCallback((city: ICity['id']) => !!pinnedCities?.includes(city), [pinnedCities]);

    const fetchItems: CityListHeaderProps['fetchItems'] = async (queries) => {
        if (queries?.countries?.length || selectedCountries?.length) {
            try {
                const params: CountryCitiesListApiRequestParamsType = {
                    countries: queries?.countries || selectedCountries?.map((c) => c.id) || [],
                    sort: queries?.sort || cityListSortFilterArray[filters.sortIndex].sort,
                    query: queries?.query ?? query,
                    page: queries?.page || filters.page,
                };

                // NOTE: Setting the loading only when the data is fetching the first page
                !queries?.append && setLoading(true);

                const [citiesResponse, adsResponse] = await Promise.all([
                    fetchCountiesCitiesList(params),
                    fetchAds({ per_page: DEFAULT_AD_COUNT_PER_REQUEST, page: queries?.page || filters.page }),
                ]);

                setCities(queries?.append ? [...(cities || []), ...citiesResponse.data] : citiesResponse.data);
                setAds(queries?.append ? [...(ads || []), ...adsResponse] : adsResponse);
                setQuery(queries?.query ?? query);
                setFilters((f) => ({
                    ...f,
                    sortIndex: queries?.sort ? cityListSortFilterArray.findIndex((s) => s.sort === queries.sort) || 0 : f.sortIndex,
                    page: queries?.page || f.page,
                }));
                setEnded(citiesResponse.meta.pagination.is_ended);

                return citiesResponse.data;
            } finally {
                setLoading(false);
            }
        }

        return [];
    };

    const loadMore: VirtualizedInfiniteScrollProps<ICityAd>['loadMore'] = async () => {
        try {
            const res = await fetchItems({ page: filters.page + 1, append: true });

            return res;
        } catch {
            return [];
        }
    };

    const renderItem: VirtualizedInfiniteScrollProps<ICityAd>['renderItem'] = (city) => (
        <CityAdItem key={city.id} item={city} pinCity={togglePin} isPinned={isCityPinned(city.id)} />
    );

    // NOTE: Resetting the page number after countries value has been changed
    useEffect(() => {
        fetchItems({
            countries: selectedCountries?.map((c) => c.id),
            page: 1,
        });

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedCountries]);

    return (
        <div className={classNames(styles['container'], className)} {...restProps}>
            <CityListHeader sortedItems={sortedItems} fetchItems={fetchItems} filters={filters} loading={loading} />

            <div className={styles['container_list']}>
                <VirtualizedInfiniteScroll
                    // NOTE: show only loading if the page number has been reset
                    items={loading ? [] : sortedItems}
                    renderItem={renderItem}
                    loadMore={loadMore}
                    isEnded={isEnded}
                />
            </div>
        </div>
    );
};
