import { ComponentPropsWithoutRef } from 'react';
import { CountryCitiesListApiRequestParamsType, IAd, ICity } from 'types';

export type CityListProps = ComponentPropsWithoutRef<'div'>;

export type ICityAd = ICity | IAd;

export type CityItemProps = { item: ICityAd; className?: string; pinCity: (city: ICity['id']) => Promise<void>; isPinned: boolean };

export type CityListHeaderProps = {
    sortedItems: Array<ICityAd>;
    fetchItems: (queries?: Partial<CountryCitiesListApiRequestParamsType & { append?: boolean }>) => Promise<Array<ICityAd>>;
    filters: { sortIndex: number; page: number };
    loading: boolean;
};
