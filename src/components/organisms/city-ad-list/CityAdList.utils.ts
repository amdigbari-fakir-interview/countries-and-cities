import { SORT_ORDERS } from 'utils/constants';

export type cityListSortFilterType = { sort?: SORT_ORDERS; label: string };
export const cityListSortFilterArray: Array<cityListSortFilterType> = [
    { sort: SORT_ORDERS.No_Sort, label: 'No Sort' },
    { sort: SORT_ORDERS.ASC, label: 'Asc Sort' },
    { sort: SORT_ORDERS.DESC, label: 'Desc Sort' },
];
