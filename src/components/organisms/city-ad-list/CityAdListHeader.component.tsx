import { Button, SearchBar, SearchBarProps, Spinner } from 'components/atoms';
import { FC } from 'react';

import { CityListHeaderProps } from './CityAdList.models';
import styles from './CityAdList.module.scss';
import { cityListSortFilterArray } from './CityAdList.utils';

export const CityListHeader: FC<CityListHeaderProps> = ({ sortedItems, fetchItems, filters, loading }) => {
    const setAsyncQuery: SearchBarProps['search'] = async (value) => {
        fetchItems({ query: value, page: 1 });
    };

    // NOTE: Infinite loop of button clicks for sort
    const nextSort = () => {
        fetchItems({ sort: cityListSortFilterArray[(filters.sortIndex + 1) % cityListSortFilterArray.length].sort, page: 1 });
    };

    return (
        <>
            <div className={styles['container_filters']}>
                <SearchBar
                    className={styles['container_filters__search-bar']}
                    id="search-cities"
                    placeholder="Search Cities"
                    search={setAsyncQuery}
                    disabled={!sortedItems?.length}
                />

                <Button disabled={!sortedItems?.length} className={styles['container_filters__sort']} onClick={nextSort}>
                    {cityListSortFilterArray[filters.sortIndex].label}
                </Button>
            </div>

            {loading && (
                <div className={styles['container_loading']}>
                    <Spinner />
                </div>
            )}
        </>
    );
};
