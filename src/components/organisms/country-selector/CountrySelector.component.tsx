import { Checkbox, Spinner } from 'components/atoms';
import { DropdownSelector, DropdownSelectorProps } from 'components/molecules';
import { FC, useContext, useEffect, useState } from 'react';
import { CountriesSetValueContext, CountriesValueContext } from 'stores/countries';
import { ICountryWithoutCities } from 'types';
import { fetchCountiesList } from 'utils/apis';

import { CountrySelectorProps } from './CountrySelector.models';
import styles from './CountrySelector.module.scss';

export const CountrySelector: FC<CountrySelectorProps> = () => {
    const { countries, selectedCountries } = useContext(CountriesValueContext);
    const { setCountries, setSelectedCountries } = useContext(CountriesSetValueContext);

    const [query, setQuery] = useState('');
    const [loading, setLoading] = useState(false);

    const search: DropdownSelectorProps<ICountryWithoutCities>['search'] = async (value) => {
        // NOTE: Here we set the query. Then listen for the query and countries list change to generate filtered list.
        setQuery(value);
        return [];
    };

    const getTitle: DropdownSelectorProps<ICountryWithoutCities>['getTitle'] = (item) => item.title;

    const renderItem: NonNullable<DropdownSelectorProps<ICountryWithoutCities>['renderItem']> = (item, selected = false) => (
        <div className={styles['container_country-item']}>
            <div className={styles['container_country-item__title-avatar']}>
                {/* {item.flag && <Avatar src={item.flag} alt={`${item.title} flag`} />} */}

                <span>{item.title}</span>
            </div>

            <Checkbox selected={selected} />
        </div>
    );

    useEffect(() => {
        setLoading(true);
        fetchCountiesList({ query })
            .then(setCountries)
            .finally(() => setLoading(false));

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query]);

    return (
        <div className={styles['container']}>
            <DropdownSelector
                items={countries || []}
                selectedItems={selectedCountries}
                setSelectedItems={setSelectedCountries}
                getTitle={getTitle}
                renderItem={renderItem}
                id="country-selector"
                inputProps={{ name: 'country-selector', placeholder: 'Search Countries' }}
                label="Countries"
                fullWidth
                search={search}
                onClear={setSelectedCountries}
            />

            {!!loading && (
                <div className={styles['container_loading']}>
                    <Spinner className={styles['container_loading__spinner']} />
                </div>
            )}
        </div>
    );
};
