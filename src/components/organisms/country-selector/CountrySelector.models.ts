import { ComponentPropsWithoutRef } from 'react';

export type CountrySelectorProps = ComponentPropsWithoutRef<'div'> & {};
