import { ComponentPropsWithoutRef } from 'react';

export type ListItemProps = Omit<ComponentPropsWithoutRef<'li'>, 'onClick'> & {
    onClick: ComponentPropsWithoutRef<'button'>['onClick'];
};
