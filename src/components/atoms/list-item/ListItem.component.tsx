import classNames from 'classnames';
import { FC } from 'react';

import { ListItemProps } from './ListItem.models';
import styles from './ListItem.module.scss';

export const ListItem: FC<ListItemProps> = ({ children, className, onClick, ...restProps }) => {
    return (
        <li className={classNames(styles['list-item'], className)} {...restProps}>
            <button onClick={onClick} className={styles['list-item_button']} type="button">
                {children}
            </button>
        </li>
    );
};
