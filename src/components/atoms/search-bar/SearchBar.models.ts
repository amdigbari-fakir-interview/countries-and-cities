import { ComponentPropsWithoutRef } from 'react';

export type SearchBarProps = Omit<ComponentPropsWithoutRef<'input'>, 'id'> & {
    searchDelay?: number;
    id: string;
    search: (value: string) => Promise<unknown>;
    fullWidth?: boolean;
    onClear?: () => void;
};
