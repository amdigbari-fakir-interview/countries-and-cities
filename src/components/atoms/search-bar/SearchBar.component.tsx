import classNames from 'classnames';
import Image from 'next/image';
import { ComponentPropsWithoutRef, ForwardRefRenderFunction, RefObject, forwardRef, useCallback, useMemo, useRef, useState } from 'react';
import { useUpdateEffect } from 'react-use';
import { FunctionType } from 'types';
import { debounce } from 'utils/invocation';

import { BaseInput, BaseInputProps } from '../base-input';
import { Spinner } from '../spinner';
import { SearchBarProps } from './SearchBar.models';
import styles from './SearchBar.module.scss';

export const ForwardedRefSearchBar: ForwardRefRenderFunction<HTMLInputElement, SearchBarProps> = (
    { id, value = '', searchDelay = 500, search, fullWidth = false, className, onClear, ...restProps },
    ref,
) => {
    // NOTE: Used this approach for the ref to support both passing and not passing the ref value
    const _ref = useRef<HTMLInputElement>(null);
    const _inputRef = (ref ?? _ref) as RefObject<HTMLInputElement>;

    const previousQueryRef = useRef('');

    const [query, setQuery] = useState(value.toString());
    const [loading, setLoading] = useState(false);

    const searchQuery = useCallback(
        (value: typeof query) => {
            setLoading(true);
            search(value.toString()).finally(() => setLoading(false));
        },
        [search],
    );

    const searchWithDebounce = useMemo(
        () => debounce(searchQuery as FunctionType, searchDelay) as typeof searchQuery,
        [searchQuery, searchDelay],
    );

    const handleChange: BaseInputProps['onChange'] = (event) => {
        setQuery?.(event.target?.value);
    };

    const clearSearch: ComponentPropsWithoutRef<'button'>['onClick'] = (event) => {
        event.stopPropagation();
        setQuery('');
        onClear?.();
    };

    const submitHandler: BaseInputProps['onSubmit'] = (value) => {
        searchWithDebounce(value);
    };

    useUpdateEffect(() => {
        query !== previousQueryRef.current && searchWithDebounce(query);

        previousQueryRef.current = query;
    }, [query]);

    useUpdateEffect(() => void (value !== undefined && setQuery(value.toString())), [value]);

    return (
        <div className={classNames(styles['container'], className, { 'w-100': fullWidth })}>
            <BaseInput
                {...restProps}
                id={id}
                className={styles['container_input']}
                value={query}
                onChange={handleChange}
                onSubmit={submitHandler}
                ref={_inputRef}
            />

            {loading && <Spinner className={classNames(styles['container_icon'], styles['container_loading'])} id={`${id}-spinner`} />}

            {!loading && !!query.length && (
                <button onClick={clearSearch} type="button" className={classNames(styles['container_icon'], styles['container_clear'])}>
                    <Image src="/assets/icons/close-circle.svg" alt="clear-icon" fill />
                </button>
            )}
        </div>
    );
};
export const SearchBar = forwardRef(ForwardedRefSearchBar);
