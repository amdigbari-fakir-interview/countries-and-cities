import classNames from 'classnames';
import { ForwardRefRenderFunction, forwardRef } from 'react';

import { BaseInputProps } from './BaseInput.models';
import styles from './BaseInput.module.scss';

const ForwardedRefBaseInput: ForwardRefRenderFunction<HTMLInputElement, BaseInputProps> = (
    { onSubmit, className, onKeyUp, ...restProps },
    ref,
) => {
    // NOTE: calling the onSubmit in case Enter clicked
    const handleOnKeyUp: BaseInputProps['onKeyUp'] = (event) => {
        onKeyUp?.(event);

        const value = (event?.target as HTMLInputElement).value;

        if (onSubmit && (event.key === 'Enter' || event.key == 'NumpadEnter')) {
            onSubmit?.(value);
        }
    };

    return (
        <input
            type="text"
            autoComplete="off"
            className={classNames(className, styles.input)}
            onKeyUp={handleOnKeyUp}
            {...restProps}
            ref={ref}
        />
    );
};
export const BaseInput = forwardRef(ForwardedRefBaseInput);
