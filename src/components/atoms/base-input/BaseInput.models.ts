import { ComponentPropsWithoutRef } from 'react';

export type BaseInputProps = Omit<ComponentPropsWithoutRef<'input'>, 'onSubmit'> & {
    onSubmit?: (value: string) => void;
};
