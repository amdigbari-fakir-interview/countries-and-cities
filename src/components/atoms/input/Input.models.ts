import { ComponentPropsWithoutRef, ReactNode } from 'react';

export type InputProps = Omit<ComponentPropsWithoutRef<'input'>, 'onSubmit' | 'onChange'> & {} & {
    label: string;
    id: string;
    fullWidth?: boolean;
    onChange?: (a: string) => void;
    onSubmit?: (msg: string) => void;
    onClear?: () => void;
    icon?: ReactNode;
    forceFocus?: boolean;
};
