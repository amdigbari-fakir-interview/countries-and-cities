import classNames from 'classnames';
import Image from 'next/image';
import { ComponentPropsWithoutRef, ForwardRefRenderFunction, RefObject, forwardRef, useRef, useState } from 'react';

import { BaseInput } from '../base-input';
import { InputProps } from './Input.models';
import styles from './Input.module.scss';

const ForwardedRefInput: ForwardRefRenderFunction<HTMLInputElement, InputProps> = (
    // NOTE: The placeholder value by default is not an empty string, because empty string makes the label move to the top
    { placeholder = ' ', label, fullWidth, id, onChange, className, onClear, onSubmit, icon, value, forceFocus = false, ...restProps },
    ref,
) => {
    // NOTE: Used this approach for the ref to support both controlled and uncontrolled the ref value
    const _ref: RefObject<HTMLInputElement> = useRef(null);
    const inputElementRef = (ref ?? _ref) as RefObject<HTMLInputElement>;

    // NOTE: Used this type instead of InputProps['onChange'] because the type has been rewritten
    const handleOnChange: ComponentPropsWithoutRef<'input'>['onChange'] = (event) => {
        onChange?.(event.target.value);
    };

    const handleClearClick: ComponentPropsWithoutRef<'button'>['onClick'] = (event) => {
        event.preventDefault();
        event.stopPropagation();

        inputElementRef.current?.focus();
        inputElementRef.current!.value = '';

        onChange?.('');
        onClear?.();
    };

    return (
        <div className={classNames(styles.container, className)}>
            <div className={classNames(styles.wrapper, { 'w-full': fullWidth, [styles['wrapper__force-focus']]: forceFocus })}>
                <BaseInput
                    {...restProps}
                    type="text"
                    autoComplete="off"
                    className={classNames(className, styles.input, { 'w-full': fullWidth })}
                    id={id}
                    value={value}
                    placeholder={placeholder}
                    onChange={handleOnChange}
                    ref={inputElementRef}
                />
                {!(!label || !label.trim()) && (
                    <label data-testid={`${id}-label`} className={styles['wrapper__label']} htmlFor={id}>
                        {label}
                    </label>
                )}

                {!!icon && (
                    <div data-testid={`${id}-icon`} className={classNames(styles['wrapper__icon'])}>
                        {icon}
                    </div>
                )}
                {!icon && !!value?.toString().trim()?.length && (
                    <button
                        type="button"
                        // NOTE: Used onMouseDown so this function calls before the input Blur
                        // NOTE: If Blur calls first, This tag will be removed from DOM before calling the click function
                        onMouseDown={handleClearClick}
                        className={classNames(styles['wrapper__icon'], styles['wrapper__icon_close'])}>
                        <Image src="/assets/icons/close-circle.svg" alt="clear-icon" fill />
                    </button>
                )}

                <fieldset data-testid={`${id}-fieldset`}>
                    {/* <fieldset data-testid='fieldset' style={{ backgroundColor: bgColor }}> */}
                    <legend className={classNames({ [styles['hide-legend']]: !label?.trim() })}>{label}</legend>
                </fieldset>
            </div>
        </div>
    );
};
export const Input = forwardRef(ForwardedRefInput);
