import { ComponentPropsWithoutRef } from 'react';

export type ButtonProps = ComponentPropsWithoutRef<'button'> & {
    fullWidth?: boolean;
    loading?: boolean;
};
