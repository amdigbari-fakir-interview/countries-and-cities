import classNames from 'classnames';
import { FC } from 'react';

import { Spinner } from '../spinner';
import { ButtonProps } from './Button.models';
import styles from './Button.module.scss';

export const Button: FC<ButtonProps> = ({ loading = false, className, children, fullWidth, disabled = false, ...restProps }) => (
    <button
        type="button"
        className={classNames(styles['button'], { 'w-full': fullWidth }, className)}
        {...restProps}
        disabled={loading || disabled}>
        <span className={classNames({ [styles['button_loading-content']]: loading })}>{children}</span>

        {loading && (
            <div className={styles['button_loading']}>
                <Spinner />
            </div>
        )}
    </button>
);
