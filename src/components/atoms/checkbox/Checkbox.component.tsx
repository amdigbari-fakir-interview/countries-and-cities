import classNames from 'classnames';
import { FC } from 'react';

import { CheckboxProps } from './Checkbox.models';
import styles from './Checkbox.module.scss';

export const Checkbox: FC<CheckboxProps> = ({ selected, className }) => {
    return <div className={classNames(styles['container'], { [styles['container_selected']]: selected }, className)} />;
};
