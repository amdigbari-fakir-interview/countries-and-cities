export type CheckboxProps = {
    selected?: boolean;
    className?: string;
};
