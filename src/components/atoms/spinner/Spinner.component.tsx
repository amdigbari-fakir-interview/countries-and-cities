import classNames from 'classnames';
import { FC } from 'react';
import { COLORS } from 'utils/constants';

import { SpinnerProps } from './Spinner.models';
import styles from './Spinner.module.scss';

export const Spinner: FC<SpinnerProps> = ({ width = 16, height = 16, className, ...restProps }) => {
    return (
        <svg width={width} height={height} {...restProps} className={classNames(styles.indeterminate, className)} viewBox="0 0 24 24">
            <g className={styles['indeterminate-g']}>
                <path
                    className={styles['indeterminate-path']}
                    d="M12,3a9,9,0,1,1,0,18a9,9,0,1,1,0-18"
                    fill="none"
                    stroke={COLORS.PRIMARY}
                    strokeWidth="2"></path>
            </g>
        </svg>
    );
};
