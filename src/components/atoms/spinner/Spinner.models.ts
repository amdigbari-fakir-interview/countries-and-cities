import { ComponentPropsWithoutRef } from 'react';

export type SpinnerProps = Omit<ComponentPropsWithoutRef<'svg'>, 'viewBox'> & {
    className?: string;
};
