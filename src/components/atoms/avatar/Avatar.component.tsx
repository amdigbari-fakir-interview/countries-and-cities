import classNames from 'classnames';
import Image from 'next/image';
import { FC } from 'react';

import { AvatarProps } from './Avatar.models';
import styles from './Avatar.module.scss';

// NOTE: This component implemented to show a mock image for each City or country at first
export const Avatar: FC<AvatarProps> = ({ containerClassName, alt, size = 24, ...restProps }) => (
    <div className={classNames(styles['avatar'], containerClassName)} style={{ width: size, height: size }}>
        <Image {...restProps} alt={alt} fill />
    </div>
);
