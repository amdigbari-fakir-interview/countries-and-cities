import { ImageProps } from 'next/image';

export type AvatarProps = Omit<ImageProps, 'width' | 'height' | 'fill'> & { containerClassName?: string; size?: number };
