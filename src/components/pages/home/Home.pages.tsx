import classNames from 'classnames';
import { CityList } from 'components/organisms/city-ad-list';
import { CountrySelector } from 'components/organisms/country-selector';
import { FC } from 'react';

import styles from './Home.module.scss';

export const HomePage: FC = () => {
    return (
        <main className={classNames('w-full', styles['container'])}>
            <div className={styles['container_countries']}>
                <CountrySelector />
            </div>

            <div className={styles['container_cities']}>
                <CityList />
            </div>
        </main>
    );
};
