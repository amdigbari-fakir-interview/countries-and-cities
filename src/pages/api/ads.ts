import { AdsDatabase } from 'database';
import type { NextApiRequest, NextApiResponse } from 'next';
import { DEFAULT_AD_COUNT_PER_REQUEST } from 'utils/constants';

export default function handler(req: NextApiRequest, res: NextApiResponse) {
    const { per_page = `${DEFAULT_AD_COUNT_PER_REQUEST}`, page = '1' } = req.query;

    const perPage = Number(per_page);
    const pageNumber = Number(page);
    if (!perPage || !pageNumber) {
        res.status(400).end();
    }

    res.status(200).json({
        status: true,
        data: AdsDatabase.data.map((ad) => ({ ...ad, isAd: true })).slice((pageNumber - 1) * perPage, pageNumber * perPage),
    });
}
