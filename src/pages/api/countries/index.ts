import { Database } from 'database';
import type { NextApiRequest, NextApiResponse } from 'next';

export default function handler(req: NextApiRequest, res: NextApiResponse) {
    const { query = '' } = req.query;

    res.status(200).json({
        status: true,
        data: Database.data
            .map((c) => ({ id: c.country, title: c.country }))
            .filter(({ title }) => title.toLowerCase().includes(query.toString().trim().toLowerCase())),
    });
}
