import { Database } from 'database';
import type { NextApiRequest, NextApiResponse } from 'next';

export default function handler(req: NextApiRequest, res: NextApiResponse) {
    const { countries = [], query = '' } = req.query;

    const countriesList = typeof countries === 'string' ? [countries] : countries;

    res.status(200).json({
        status: true,
        data: Database.data
            .filter((country) => countriesList.includes(country.country))
            .flatMap((country) => country.cities.map((city) => ({ id: `${country.country}-${city}`, title: city, isAd: false })))
            .filter(({ title }) => title.toLocaleLowerCase().includes(query.toString().trim().toLocaleLowerCase())),
    });
}
