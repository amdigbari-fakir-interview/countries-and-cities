import { HomePage } from 'components/pages/home/Home.pages';
import { CitiesContext } from 'stores/cities';
import { CountriesContext } from 'stores/countries';

export default function Home() {
    return (
        <CountriesContext>
            <CitiesContext>
                <HomePage />
            </CitiesContext>
        </CountriesContext>
    );
}
