import { ICity } from './city.models';

export interface ICountry {
    id: string;
    title: string;

    cities: Array<ICity>;

    // flag?: string;
}

export type ICountryWithoutCities = Omit<ICountry, 'cities'>;

export type CountriesListApiResponseType = {
    status: true;
    data: Array<ICountryWithoutCities>;
};

export type CountriesListApiRequestParamsType = {
    query?: string;
};
