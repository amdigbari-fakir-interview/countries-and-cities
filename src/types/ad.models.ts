export interface IAd {
    id: string;
    title: string;
    isAd: true;
}

export type AdListApiResponseType = {
    status: true;
    data: Array<IAd>;
};

export type AdListApiRequestParamsType = {
    per_page: number;
    page: number;
};
