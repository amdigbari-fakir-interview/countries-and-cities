import { SORT_ORDERS } from 'utils/constants';

import { ICountry } from './country.models';

export interface ICity {
    id: string;
    title: string;

    isAd: false;

    // image?: string;

    // NOTE: I believe Pinned is sth that should be stored on the database like the Telegram.
    // NOTE: Based on that, I added this property to the city type at first.
    // pinned?: boolean;
}

export type CountryCitiesListApiResponseType = {
    status: true;
    data: Array<ICity>;
    meta: { pagination: { is_ended: boolean } };
};

export type CountryCitiesListApiRequestParamsType = {
    countries: Array<ICountry['id']>;
    sort?: SORT_ORDERS;
    query?: string;
    page?: number;
};

export type LocalStoragePinnedCitiesType = Array<ICity['id']>;
