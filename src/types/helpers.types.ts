export type FunctionType = (...args: unknown[]) => unknown;

export type GenericObjectBaseType = { id: string };
