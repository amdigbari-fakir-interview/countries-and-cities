import { faker } from '@faker-js/faker';
import { ICity } from 'types';

// NOTE: Used this functions to mock the data. I didn't remove them to show the process
// NOTE: because it's a hiring task. In actual projects, this kind of codes will be removed.
export function getCityFakeData(): ICity {
    return {
        id: faker.datatype.uuid(),
        title: faker.address.city(),
        isAd: false,
        // image: faker.image.city(),
    };
}

export function getCountryCitiesFakeData(): Array<ICity> {
    return new Array(100).fill(0).map(getCityFakeData);
}
