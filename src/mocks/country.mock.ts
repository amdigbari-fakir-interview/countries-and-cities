import { faker } from '@faker-js/faker';
import { ICountry } from 'types';

// NOTE: Used this functions to mock the data. I didn't remove them to show the process
// NOTE: because it's a hiring task. In actual projects, this kind of codes will be removed.
export function getCountryFakeData(): ICountry {
    return {
        id: faker.datatype.uuid(),
        title: faker.address.country(),
        // flag: faker.image.city(),

        cities: [],
    };
}
