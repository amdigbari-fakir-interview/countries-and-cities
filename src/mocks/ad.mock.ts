import { faker } from '@faker-js/faker';
import { IAd } from 'types';

// NOTE: Used this functions to mock the data. I didn't remove them to show the process
// NOTE: because it's a hiring task. In actual projects, this kind of codes will be removed.
export function getAdFakeData(): IAd {
    return {
        id: faker.datatype.uuid(),
        title: faker.address.cityName(),
        isAd: true,
    };
}
