import { IAd, ICity } from 'types';

export type CitiesContextValueType = {
    cities?: Array<ICity>;
    ads?: Array<IAd>;
};

export type CitiesContextSetValueType = {
    setCities: (cities: CitiesContextValueType['cities']) => void;
    setAds: (ads: CitiesContextValueType['ads']) => void;
};
