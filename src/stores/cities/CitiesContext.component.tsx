import { FC, PropsWithChildren, createContext, useState } from 'react';

import { CitiesContextSetValueType, CitiesContextValueType } from './CitiesContext.models';

const defaultValue: CitiesContextValueType = { cities: undefined, ads: undefined };
export const CitiesValueContext = createContext<CitiesContextValueType>(defaultValue);

export const CitiesSetValueContext = createContext<CitiesContextSetValueType>({
    setCities: () => void null,
    setAds: () => void null,
});

export const CitiesContext: FC<PropsWithChildren> = ({ children }) => {
    const [cities, setCities] = useState(defaultValue.cities);
    const [ads, setAds] = useState(defaultValue.ads);

    return (
        <CitiesSetValueContext.Provider value={{ setCities, setAds }}>
            <CitiesValueContext.Provider value={{ cities, ads }}>{children}</CitiesValueContext.Provider>
        </CitiesSetValueContext.Provider>
    );
};
