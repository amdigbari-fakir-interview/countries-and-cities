import { FC, PropsWithChildren, createContext, useState } from 'react';

import { CountriesContextSetValueType, CountriesContextValueType } from './CountriesContext.models';

const defaultValue: CountriesContextValueType = { countries: undefined, selectedCountries: undefined };
export const CountriesValueContext = createContext<CountriesContextValueType>(defaultValue);

export const CountriesSetValueContext = createContext<CountriesContextSetValueType>({
    setCountries: () => void null,
    setSelectedCountries: () => void null,
});

export const CountriesContext: FC<PropsWithChildren> = ({ children }) => {
    const [countries, setCountries] = useState(defaultValue.countries);
    const [selectedCountries, setSelectedCountries] = useState(defaultValue.selectedCountries);

    return (
        <CountriesSetValueContext.Provider value={{ setCountries, setSelectedCountries }}>
            <CountriesValueContext.Provider value={{ countries, selectedCountries }}>{children}</CountriesValueContext.Provider>
        </CountriesSetValueContext.Provider>
    );
};
