import { ICountryWithoutCities } from 'types';

export type CountriesContextValueType = {
    countries?: Array<ICountryWithoutCities>;

    selectedCountries?: Array<ICountryWithoutCities>;
};

export type CountriesContextSetValueType = {
    setCountries: (countries: CountriesContextValueType['countries']) => void;
    setSelectedCountries: (countries?: Array<ICountryWithoutCities>) => void;
};
