/** @type {import('next').NextConfig} */
const path = require('path');

const nextConfig = {
    output: 'standalone',
    reactStrictMode: true,
    trailingSlash: false,
    // NOTE: Added this for the mock Avatars
    images: { domains: (process.env.NEXT_PUBLIC_IMAGES_DOMAINS || '').split(',').map((d) => d.trim()) },
    // NOTE: The sassOptions make it happen to import helpers instead of relative path to src/styles/helpers/helpers
    sassOptions: { includePaths: [path.resolve(__dirname, 'src', 'styles', 'helpers')] },
    devIndicators: { buildActivity: true, buildActivityPosition: 'bottom-right' },
};

module.exports = nextConfig;
